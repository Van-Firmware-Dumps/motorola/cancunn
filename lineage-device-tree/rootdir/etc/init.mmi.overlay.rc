# NFC ST21NFC
import /vendor/etc/init/hw/init.vendor.st21nfc.rc

on property:ro.vendor.hw.dualsim=true
    setprop persist.vendor.radio.multisim.config dsds

on property:ro.vendor.hw.dualsim=false
    setprop persist.vendor.radio.multisim.config ""

on early-init
    insmod /vendor/lib/modules/sensors_class.ko
    insmod /vendor/lib/modules/sx937x_sar.ko
    exec_background u:r:vendor_modprobe:s0 -- /vendor/bin/modprobe -a -d /vendor/lib/modules qpnp_adaptive_charge aw9610x leds-gpio st54nfc moto_mmap_fault cps4038_mmi
    exec u:r:vendor_modprobe:s0 -- /vendor/bin/modprobe -r -d /vendor/lib/modules zram

on post-fs-data
    # calibration
    mkdir /data/vendor/sensor 0774 system system
    # Sensor
    chmod 0660 /dev/hf_manager
    chown system system /dev/hf_manager

    # config fingerprint
    mkdir /data/vendor/.fps 0770 system vendor_fingerp
    mkdir /mnt/vendor/persist/fps 0770 system system
    mkdir /data/vendor/egis 0770 system vendor_fingerp
    mkdir /data/vendor/egis/cqa 0770 system vendor_fingerp
    mkdir /mnt/vendor/persist/egis 0770 system vendor_fingerp

    # Required by touchRec for write the touch data
    mkdir /data/vendor/touchrec 0770 input input
    chown input input /data/vendor/touchrec/bootindex
    chown input input /data/vendor/touchrec/lastbootuuid
    chown input input /data/vendor/touchrec/touch_data.txt
    chmod 0664 /data/vendor/touchrec/touch_data.txt

    # change dir permission
    mkdir /data/vendor/camera_dump
    chown root camera /data/vendor/camera_dump
    chmod 0770 /data/vendor/camera_dump

service vendor.modem-rfs-sh /vendor/bin/init.mmi.modem-rfs.sh loop 600
    class late_start
    user root
    group log system radio
    oneshot

on init
# Create top-app/foreground BFQ blkio group and apply initial settings.
    mkdir /dev/blkio/foreground
    mkdir /dev/blkio/top-app
    chown system system /dev/blkio/foreground
    chown system system /dev/blkio/foreground/tasks
    chown system system /dev/blkio/foreground/cgroup.procs
    chown system system /dev/blkio/top-app
    chown system system /dev/blkio/top-app/tasks
    chown system system /dev/blkio/top-app/cgroup.procs
    chmod 0664 /dev/blkio/foreground/tasks
    chmod 0664 /dev/blkio/foreground/cgroup.procs
    chmod 0664 /dev/blkio/top-app/tasks
    chmod 0664 /dev/blkio/top-app/cgroup.procs
    write /dev/blkio/foreground/blkio.bfq.weight 200
    write /dev/blkio/top-app/blkio.bfq.weight 400

on late-init
    #moto algo params
    chmod 0660 /sys/bus/platform/drivers/mtk_nanohub/algo_params
    chown system system /sys/bus/platform/drivers/mtk_nanohub/algo_params

    chown system system /sys/class/sensors/capsense_bottom_left/enable
    chown system system /sys/class/sensors/capsense_bottom_left/poll_delay
    chown system system /sys/class/sensors/capsense_bottom_right/enable
    chown system system /sys/class/sensors/capsense_bottom_right/poll_delay
    chown system system /sys/class/sensors/capsense_top_left/enable
    chown system system /sys/class/sensors/capsense_top_left/poll_delay
    chown system system /sys/class/sensors/capsense_top_mid/enable
    chown system system /sys/class/sensors/capsense_top_mid/poll_delay
    chown system system /sys/class/sensors/capsense_top_right/enable
    chown system system /sys/class/sensors/capsense_top_right/poll_delay
    exec u:r:vendor_modprobe:s0 -- /vendor/bin/modprobe -a -d /vendor/lib/modules moto_mm moto_swap
    exec_background u:r:vendor_modprobe:s0 -- /vendor/bin/modprobe -a -d /vendor/lib/modules aw862x.ko

service capsense_reset /vendor/bin/capsense_reset
    class core
    user system
    group system input
    task_profiles HighEnergySaving ProcessCapacityLow LowIoPriority TimerSlackHigh
    disabled

on property:sys.boot_completed=1
    start capsense_reset
    chown system system /sys/class/touchscreen/primary/stylus_mode
    chmod 0660 /sys/class/touchscreen/primary/stylus_mode

# Turn on led to indicate device on in factory mode
on property:ro.bootmode=mot-factory
    write /sys/class/leds/charging/brightness 255
    chmod 0666 /proc/cts_tool

on early-boot
    # Permission for Health Storage HAL
    chown system system /sys/devices/platform/soc/112b0000.ufshci/manual_gc
    chown system system /sys/devices/platform/soc/112b0000.ufshci/ufshid/trigger
    # Create directory for wireless charge test in factory
    mkdir /data/vendor/wlc 0775 vendor_tcmd system

on boot
    write /sys/class/i2c-dev/i2c-3/device/3-0038/wireless_fw_update 1
    # Set wls perms for HAL
    chown system system /sys/class/power_supply/wireless/device/pen_control
    chown system system /sys/class/power_supply/wireless/device/tx_mode
    chown system system /sys/class/power_supply/wireless/device/wls_input_current_limit
    chown system system /sys/class/power_supply/wireless/device/folio_mode
    chmod 0660 /sys/class/power_supply/wireless/device/pen_control
    chmod 0660 /sys/class/power_supply/wireless/device/tx_mode
    chmod 0660 /sys/class/power_supply/wireless/device/wls_input_current_limit
    chmod 0660 /sys/class/power_supply/wireless/device/folio_mode
    # change permission for capsensor
    chown system system /sys/class/capsense/reset
    chown system system /sys/class/capsense/int_state
    chown radio system /sys/class/capsense/reg
    chown radio system /sys/class/capsense/fw_download_status
    chmod 0660 /sys/class/capsense/reset
    chmod 0660 /sys/class/capsense/int_state
    chmod 0660 /sys/class/capsense/reg
    chmod 0660 /sys/class/capsense/fw_download_status

    # change permission for nfc
    chmod 0666 /dev/st21nfc
    # Change ownership and permission for cp-standalone factory testing
    chown system system /sys/class/power_supply/cp-standalone/voltage_now
    chown system system  /sys/class/power_supply/cp-standalone/device/force_chg_auto_enable
    chmod 0644 /sys/class/power_supply/cp-standalone/voltage_now
    # Set adaptive charging perms for HAL
    chown system system /sys/module/qpnp_adaptive_charge/parameters/upper_limit
    chown system system /sys/module/qpnp_adaptive_charge/parameters/lower_limit
    # touch api
    chown system system /sys/class/touchscreen/primary/interpolation
    chmod 0660 /sys/class/touchscreen/primary/interpolation
    chown system system /sys/class/touchscreen/primary/first_filter
    chmod 0660 /sys/class/touchscreen/primary/first_filter
    chown system system /sys/class/touchscreen/primary/edge
    chmod 0660 /sys/class/touchscreen/primary/edge
    chown system system /sys/class/touchscreen/primary/gesture
    chmod 0660 /sys/class/touchscreen/primary/gesture

    # set aw smart pa node can be accessed by audio group
    chown root audio /sys/devices/platform/11ed4000.i2c/i2c-9/9-0034/cali_re
    chmod 0664 /sys/devices/platform/11ed4000.i2c/i2c-9/9-0034/cali_re

    # Set wlc perms for chg
    chown vendor_tcmd system /sys/class/power_supply/wireless/device/tx_mode_vout

    # Set wlc perms for HAL
    chown system system /sys/class/power_supply/wireless/device/wlc_light_ctl
    chown system system /sys/class/power_supply/wireless/device/wlc_fan_speed
    chown system system /sys/class/power_supply/wireless/device/wlc_st_changed
    chown system system /sys/class/power_supply/wireless/device/wlc_tx_power
    chown system system /sys/class/power_supply/wireless/device/wlc_tx_type
    chown system system /sys/class/power_supply/wireless/device/wlc_tx_sn
    chown system system /sys/class/power_supply/wireless/device/wlc_tx_capability
    chown system system /sys/class/power_supply/wireless/device/wlc_tx_id
    chmod 0660 /sys/class/power_supply/wireless/device/wlc_light_ctl
    chmod 0660 /sys/class/power_supply/wireless/device/wlc_fan_speed

    write /sys/block/sdc/queue/scheduler bfq
    write /sys/block/sdc/queue/iosched/slice_idle 0

    write /proc/sys/vm/dirty_background_bytes 26214400
    write /proc/sys/vm/dirty_bytes 104857600

    # Set log level of mtk_gauge as BMLOG_INFO_LEVEL(6)
    write /sys/devices/platform/11b20000.i2c/i2c-5/5-0034/11b20000.i2c:mt6375@34:mtk_gauge/power_supply/battery/log_level 6

on fs
   exec_background u:r:vendor_modprobe:s0 -- /vendor/bin/modprobe -a -d /vendor/lib/modules mmi_relay nova_0flash_mmi_v2 focaltech_0flash_mmi_v2 ilitek_v3_mmi chipone_tddi_mmi_v2

on property:ro.bootmode=mot-factory
    write /sys/class/leds/charging/brightness 255

on property:sys.boot_completed=1
    chmod 0644 /data/vendor/camera_dump/mot_gt24p128f_ov50d_eeprom.bin
    chmod 0644 /data/vendor/camera_dump/serial_number_main.bin
    chmod 0644 /data/vendor/camera_dump/mot_gt24p64ba2_hi1634q_eeprom.bin
    chmod 0644 /data/vendor/camera_dump/serial_number_front.bin
    chmod 0644 /data/vendor/camera_dump/mot_cancunn_s5k4h7_otp.bin
    chmod 0644 /data/vendor/camera_dump/serial_number_wide.bin

on moto-post-fs-data-fs-tune
    write /sys/block/${dev.mnt.dev.system_ext}/queue/read_ahead_kb 2048
    write /sys/block/${dev.mnt.dev.vendor}/queue/read_ahead_kb 2048
    write /sys/block/${dev.mnt.dev.product}/queue/read_ahead_kb 2048
    write /sys/block/${dev.mnt.dev.data}/queue/read_ahead_kb 2048
    write /sys/block/${dev.mnt.dev.root}/queue/read_ahead_kb 2048
    write /sys/block/${dev.mnt.dev.vendor_dlkm}/queue/read_ahead_kb 2048

on moto-boot-completed-fs-tune
    write /sys/block/dm-0/queue/read_ahead_kb 512
    write /sys/block/dm-1/queue/read_ahead_kb 512
    write /sys/block/dm-2/queue/read_ahead_kb 512
    write /sys/block/dm-3/queue/read_ahead_kb 512
    write /sys/block/dm-4/queue/read_ahead_kb 512
    write /sys/block/dm-5/queue/read_ahead_kb 512
    write /sys/block/${dev.mnt.dev.system_ext}/queue/read_ahead_kb 512
    write /sys/block/${dev.mnt.dev.vendor}/queue/read_ahead_kb 512
    write /sys/block/${dev.mnt.dev.product}/queue/read_ahead_kb 512
    write /sys/block/${dev.mnt.dev.data}/queue/read_ahead_kb 512
    write /sys/block/${dev.mnt.dev.root}/queue/read_ahead_kb 512
    write /sys/block/${dev.mnt.dev.vendor_dlkm}/queue/read_ahead_kb 512

# === DEBUGGING FEATURE ===
# Redirect LOG[E|W|I] logs to uart in case system server keeps rebooting without adb connection
# Pass logcat buffer names to androidboot.loge2uart by cmdl under fastboot.
# For example: fastboot oem config cmdl "androidboot.loge2uart=system,main"
# Of course, console must be enabled to make this work
service loge2uart /system/bin/logcat -b ${ro.boot.loge2uart} -v threadtime -f /dev/ttyS0 *:E
    user root
    group root
    disabled

service logw2uart /system/bin/logcat -b ${ro.boot.logw2uart} -v threadtime -f /dev/ttyS0 *:W
    user root
    group root
    disabled

service logi2uart /system/bin/logcat -b ${ro.boot.logi2uart} -v threadtime -f /dev/ttyS0 *:I
    user root
    group root
    disabled

on late-init && property:ro.boot.loge2uart=*
    setprop persist.vendor.uartconsole.enable 1
    start loge2uart

on late-init && property:ro.boot.logw2uart=*
    setprop persist.vendor.uartconsole.enable 1
    start logw2uart

on late-init && property:ro.boot.logi2uart=*
    setprop persist.vendor.uartconsole.enable 1
    start logi2uart
